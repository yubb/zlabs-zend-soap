<?php

try {    
	
	ini_set('soap.wsdl_cache_enabled', '0'); 
	ini_set('soap.wsdl_cache_ttl', '0'); 

	//$url = 'http://api-dev.zlien.local/sandbox/soap/wsdl';
	$url = 'http://api-dev.zlien.local/soap/nwl/wsdl';
	
	$options = array(
		//'trace' => true,
		'soap_version' => SOAP_1_1,
		'cache_wsdl' => WSDL_CACHE_NONE
	);
	
	$client = new SoapClient($url, $options);
	
	//var_dump($client->__getFunctions());
	//var_dump($client->__getTypes());
	
	$params_array = array(
		'api_key' => '336138691f9b0ad35d905b513106ad18a485dab4ef31c5f17a50cef8075df4fa',    
		'api_secret' => 'ecdfe9a4b103a53c8d5a040c75f7aef323ce0fe849e7673092190442b409c4bc' 
	);
	
	$result = $client->Login($params_array);    
	$token = $result->LoginResult;    
	
	
	// *** Add a user
	
	$user = new stdClass();
	
	$user->user_first_name = 'Barack';
	$user->user_last_name = 'Obama' . rand();	
	$user->user_company_name = 'United States';	
	$user->user_address = '1600 Pennsylvania Ave.';
	$user->user_state = 'DC';
	$user->user_city = 'Washington';
	$user->user_zip = '20500';
	$user->user_email = 'obama'  . rand() . '@usa.gov';
	
	$params_array = array( 
		'user' => $user, 
		'token'   => $token 
	);    
	
	$result = $client->AddUser($params_array); 
	
	if($result->AddUserResult->success == false)
	{
		echo "Unable to add user: " . $result->AddUserResult->user_import_message;
		die();
	}
	
	
	// *** Add a contact
	
	$contact = new stdClass();     
	
	$contact->contact_role_name = 'Property Owner / Public Entity';   	
	$contact->contact_role_review = false;
	$contact->contact_company = 'House of Representatives ' . rand();    	
	$contact->contact_phone = '800-829-1040'; 	
	$contact->contact_state = 'DC';    	
	$contact->contact_address = 'East Capitol St NE & First St SE';    	
	$contact->contact_city = 'Washington';    
	$contact->contact_email = 'congress@usa.gov';    	
	$contact->contact_website = 'www.house.gov';    	
	$contact->contact_is_hiring_party = 1;
	$contact->arbitrary_data = array();
 
	$params_array = array( 
		'contact' => $contact, 
		'token'   => $token 
	);    
	
	$result = $client->AddContact($params_array); 
		
	if($result->AddContactResult->success == false)
	{
		echo "Unable to add contact: " . $result->AddContactResult->contact_import_message;
		die();
	}
	
	
	// *** Add a project
	
	$project_id = 'test' . rand();
				
	$project_date = new stdClass();	
	$project_date->external_project_id = $project_id;
	$project_date->project_date_type = 'Completion Date';
	$project_date->project_date = '2015-12-21';
	
	$project = new stdClass();
	
	$project->external_project_id = $project_id;
	$project->project_name = 'Example Government Project' . rand();
	$project->project_type = 'Federal';
	$project->project_type_review = false;
	$project->project_address = '9500 MacArthur Blvd.';
	$project->project_city = 'West Bethesda';
	$project->project_county = 'DC';
	$project->project_state_abbreviation = 'MD';
	$project->project_zip = '20817';
	$project->project_contract_amount = 1000000; // 1 million
	$project->project_outstanding_amount = 100000; // 100 thousand
	$project->project_client_role = 'General Contractor';
	$project->project_client_role_review = false;
	$project->project_client_role_name_review = false;
	$project->hiring_party_role = 'Property Owner / Public Entity ';
	$project->project_lob = 'Default';
	$project->project_preferences = 'ALWAYS_SEND_NOTICE';
	$project->place_order_type = 'Preliminary Notice';
	$project->project_status = 'ACTIVE';
	
	$params_array = array( 
		'project' => $project, 
		'project_dates' => array($project_date),
		'contacts' => array($contact),
		'token'   => $token 
	);    
	
	$result = $client->AddProject($params_array); 
	
	if($result->AddProjectResult->success == false)
	{
		echo "Unable to add project: " . $result->AddProjectResult->project_import_message;
		die();
	}
	
	echo "Successfully added user, contact, and project data";
	
} catch(SoapFault $e) {    	
	
	//echo $client->__getLastRequest() . '\n\n';	
	//echo $client->__getLastResponse() . '\n\n';
	
	echo 'Soap Fault ' . $e->getMessage() . '\n\n';
	
} catch(Exception $e) {    	
	
	echo 'ERROR ' . $e->getMessage() . '\n\n';
	
} 

?>

# zlabs-zend-soap

`zlabs/zlabs-zend-soap` is a composer package that contains some of our modifications to be able to handle data marshalling for array of arrays, and array of objects in zlien's SOAP API.

# zend-soap

`Zend\Soap` is a component to manage the [SOAP](http://en.wikipedia.org/wiki/SOAP)
protocol in order to design client or server PHP application.


